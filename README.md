# Mezos — Middleware Tezos for Mobile Devices and more

**Warning:** *Refactoring in progress*

## Description

Mezos provides a middleware for mobile devices.
Technically, it connects to a PostGres database filled by
[`tezos-indexer`](https://gitlab.com/nomadic-labs/tezos-indexer) and
provides a set of RPC for mobile devices to use.
It also connects directly to the tezos-node for some wallet operations;
and that's the reason why you can't run the same Mezos code for all Tezos networks.


## Installation

Once you have `tezos-node` and `tezos-indexer` running, it's easy to run Mezos
(from the machine that hosts `tezos-indexer`).

If you need to install its dependencies, here's the opam command:
```
opam install -y caqti-dynload caqti-lwt caqti-driver-postgresql mpp \
blake2.0.3 \
zarith.1.9.1 \
uutf.1.0.2 \
lwt-watcher.0.1 \
lwt-canceler.0.2 \
lwt_log.1.1.1 \
lwt.4.2.1 \
ocp-ocamlres.0.4 \
uecc.0.2 \
secp256k1-internal.0.2.0 \
hacl.0.3 \
digestif.0.9.0 \
ezjsonm.1.2.0 \
ipaddr.4.0.0 \
resto.0.5 \
ledgerwallet.0.1.0 \
irmin.2.1.0 \
ledgerwallet-tezos.0.1.0 \
irmin-pack.2.1.0 \
data-encoding.0.2 \
tls.0.10.6 \
resto-cohttp-client.0.5 \
resto-cohttp-server.0.5 \
genspio \
dum
```

Get the source code:
```
git clone https://gitlab.com/nomadic-labs/mezos
cd mezos
```

Build for Babylon (PsBabyM1):
```
make 5
```

Build for Carthage (PsCARTHA):
```
make 6
```

Build for Delphi (PsDELPH1):
```
make 7
```

Make sure you have the matching SQL schemes from tezos-indexer:
```
cd tezos-indexer && make db-scheme-all-default | psql yourdatabase
```

And run it:
```
./mezos.exe run
```

Don't hesitate to consult
```
./mezos.exe --help
```
