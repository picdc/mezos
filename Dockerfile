FROM alpine AS build

RUN apk add opam alpine-sdk m4 gmp-dev postgresql-dev perl libev-dev libffi libffi-dev
RUN apk add --no-cache -X http://dl-cdn.alpinelinux.org/alpine/edge/community hidapi-dev

RUN opam init --bare --disable-sandboxing
RUN opam switch create tezos ocaml-base-compiler.4.09.1 # this may take quite a while

RUN opam install -y caqti-dynload caqti-lwt caqti-driver-postgresql mpp \
blake2.0.3 \
zarith.1.9.1 \
uutf.1.0.2 \
lwt-watcher.0.1 \
lwt-canceler.0.2 \
lwt_log.1.1.1 \
lwt.4.2.1 \
ocp-ocamlres.0.4 \
uecc.0.2 \
secp256k1-internal.0.2.0 \
hacl.0.3 \
digestif.0.9.0 \
ezjsonm.1.2.0 \
ipaddr.4.0.0 \
resto.0.5 \
ledgerwallet.0.1.0 \
irmin.2.1.0 \
ledgerwallet-tezos.0.1.0 \
irmin-pack.2.1.0 \
data-encoding.0.2 \
tls.0.10.6 \
resto-cohttp-client.0.5 \
resto-cohttp-server.0.5 \
genspio \
dum

WORKDIR /mezos

RUN echo -e '[user]\nemail = email@email.com\nname = Name' > $HOME/.gitconfig

RUN git clone --single-branch https://gitlab.com/philippewang.info/tezos.git -b v7.4-mezos

COPY Makefile .
COPY src ./src
COPY dune-project .
COPY .git .
COPY *.opam ./

RUN eval $(opam env) ; make tezos-indexer
RUN eval $(opam env) ; make 6
RUN eval $(opam env) ; make 7

RUN rm -rf _build

RUN /mezos/mezos.exe


ENTRYPOINT ["/mezos/mezos.exe"]
