(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Rresult
open Cmdliner
open Cmdliner_helpers

open Tezos_protocol_007_PsDELPH1.Protocol

module Convs = struct
  let tez =
    let open Alpha_context.Tez in
    let parser v =
      match of_string v with
      | None -> Error (`Msg "Invalid tez amount")
      | Some s -> Ok s in
    Arg.conv ~docv:"tez" (parser, pp)

  let address =
    let open Tezos_crypto.Signature.Public_key_hash in
    let parser v =
      match of_b58check v with
      | Error _e -> R.error_msgf "%s is not a valid pkh" v
      | Ok addr -> Ok addr in
    Arg.conv ~docv:"pkh" (parser, pp)

  let contract =
    let open Alpha_context.Contract in
    let parser v =
      match of_b58check v with
      | Error _e -> R.error_msgf "%s is not a valid contract id" v
      | Ok addr -> Ok addr in
    Arg.conv ~docv:"address" (parser, pp)

  let public_key =
    let open Tezos_base.TzPervasives in
    let parser v =
      R.error_to_msg ~pp_error:pp_print_error
        (Signature.Public_key.of_b58check v) in
    Arg.conv ~docv:"public key" (parser, Signature.Public_key.pp)
end

module Terms = struct
  let fee =
    let doc = "Transaction fee in mutez" in
    let open Alpha_context in
    Arg.(value & opt Convs.tez Tez.zero & info ["fee"] ~doc ~docv:"mutez")
end
