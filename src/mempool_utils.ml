(*************************ver****************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019-2020 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* open Tezos_client_(\**# get PROTO#**\).Protocol_client_context *)
(* open Tezos_client_007_PsDELPH1.Protocol_client_context *)

module BS = Block_services.Make(Tezos_protocol_007_PsDELPH1.Protocol)(Tezos_protocol_007_PsDELPH1.Protocol)
module Alpha_context = Tezos_raw_protocol_007_PsDELPH1.Alpha_context

module Verbose = Tezos_indexer_lib.Verbose
module Debug = Verbose.Debug

let lazy_expr_pp out e = match Data_encoding.force_decode e with
  | None -> ()
  | Some p ->
    Format.fprintf out "%a"
      Tezos_client_007_PsDELPH1.Michelson_v1_printer.print_expr p

let operation_pp : type a. _ -> a Alpha_context.manager_operation -> unit = fun out -> function
  | Alpha_context.Reveal pk ->
    Format.fprintf out "{Reveal %a}" Signature.Public_key.pp pk
  | Transaction { amount ; parameters ; entrypoint ; destination } ->
    Format.fprintf out "{Transaction amount=%a parameters=%a entrypoint=%s destination=%a}"
      Alpha_context.Tez.pp amount
      lazy_expr_pp parameters
      entrypoint
      Alpha_context.Contract.pp destination
  | Origination { delegate ; script ; credit ; preorigination ; } ->
    Format.fprintf out "{Origination%a script=%a credit=%a%a }"
      (Verbose.Utils.pp_option ~name:"delegate" ~pp:Signature.Public_key_hash.pp) delegate
      (fun out { Alpha_context.Script.code ; storage } ->
         Format.fprintf out "{code=%a storage=%a}"
           lazy_expr_pp code
           lazy_expr_pp storage
      ) (script:Alpha_context.Script.t)
      Alpha_context.Tez.pp credit
      (Verbose.Utils.pp_option ~name:"preorigination" ~pp:Alpha_context.Contract.pp) preorigination
  | Delegation pkh_opt ->
    Format.fprintf out "{Delegation%a}"
      (Verbose.Utils.pp_option ~name:"pkh" ~pp:Signature.Public_key_hash.pp) pkh_opt

type mop =
  | R of Signature.public_key
  | T of { amount : Alpha_context.Tez.t
         ; parameters : Alpha_context.Script.lazy_expr
         ; entrypoint : string
         ; destination : Alpha_context.Contract.t
         }
  | O of { delegate : Signature.public_key_hash option
         ; script : Alpha_context.Script.t
         ; credit : Alpha_context.Tez.t
         ; preorigination : Alpha_context.Contract.t option
         ; }
  | D of Signature.public_key_hash option

let mop_of_manager_operation : type a. a Alpha_context.manager_operation -> mop = function
  | Alpha_context.Reveal pk ->
    R pk
  | Transaction { amount ; parameters ; entrypoint ; destination ; } ->
    T { amount ; parameters ; entrypoint ; destination }
  | Origination { delegate ; script ; credit ; preorigination ; } ->
    O { delegate ; script ; credit ; preorigination ; }
  | Delegation pkh_opt ->
    D pkh_opt


(* module Endorsements = struct
 *   module M = Map.Make(struct
 *       type t = Block_hash.t * Alpha_context.Raw_level.t
 *       let compare = compare
 *     end)
 *   let m = ref M.empty in
 *   let seen e = M.is_mem e !m in
 *   let register e = m := M.add e !m in
 * end *)

type status = Chain_db.Mempool_operations.status = | Applied | Refused | Branch_refused | Unprocessed | Branch_delayed

module Tez = Alpha_context.Tez

type result = {
  age: int;
  branch: Block_hash.t;
  fee: Tez.t;
  operation: mop;
  counter: Z.t;
  gas_limit: Z.t;
  storage_limit: Z.t;
  status: status;
  source: Signature.public_key_hash;
  ophash: Operation_hash.t;
  seen: float;
}
let string_of_status = function
  | Applied -> "applied"
  | Refused -> "refused"
  | Branch_refused -> "branch_refused"
  | Unprocessed -> "unprocessed"
  | Branch_delayed -> "branch_delayed"
let status_of_string = function
  | "applied" -> Some Applied
  | "refused" -> Some Refused
  | "branch_refused" -> Some Branch_refused
  | "unprocessed" -> Some Unprocessed
  | "branch_delayed" -> Some Branch_delayed
  | _ -> None


(* let manager_operation_json_encoding =
 *   let open Data_encoding in
 *   let manager_operation_encoding =
 *     conv
 *       (function
 *         | R pk ->
 *           "reveal", (Some pk, None, None, None, None, None, None, None, None, None)
 *         | T { amount ; parameters ; entrypoint ; destination ; } ->
 *           "transaction", (None, Some amount, Some parameters, Some entrypoint, Some destination, None, None, None, None, None)
 *         | O { delegate ; script ; credit ; preorigination ; } ->
 *           "origination", (None, None, None, None, None, delegate, Some script, Some credit, preorigination, None)
 *         | D pkh_opt ->
 *           "delegate", (None, None, None, None, None, None, None, None, None, pkh_opt)
 *       )
 *       (fun _ -> assert false)
 *       (merge_objs
 *          (obj1 (req "type" string))
 *          (obj10
 *             (opt "pk" Signature.Public_key.encoding)
 *             (opt "amount" Alpha_context.Tez.encoding)
 *             (opt "parameters" Alpha_context.Script.lazy_expr_encoding)
 *             (opt "entrypoint" string)
 *             (opt "destination" Alpha_context.Contract.encoding)
 *             (opt "delegate" Signature.Public_key_hash.encoding)
 *             (opt "script" Alpha_context.Script.encoding)
 *             (opt "credit" Alpha_context.Tez.encoding)
 *             (opt "preorigination" Alpha_context.Contract.encoding)
 *             (opt "pkh" Signature.Public_key_hash.encoding)
 *          )
 *       )
 *   in
 *   conv
 *     (fun { source; age; branch; fee; operation; counter; gas_limit; storage_limit; status; ophash ; seen } ->
 *        (Printf.sprintf "%f" seen,
 *         (age, string_of_status status, source, branch, fee, operation, counter, gas_limit, storage_limit, ophash)))
 *     (fun _ -> invalid_arg "Mempool_utils.json_encoding")
 *     (merge_objs
 *        (obj1 (req "seen" string))
 *        (obj10
 *           (req "age" Data_encoding.int16)
 *           (req "status" string)
 *           (req "source" Signature.Public_key_hash.encoding)
 *           (req "branch" Block_hash.encoding)
 *           (req "fee" Tez.encoding)
 *           (req "operation" manager_operation_encoding)
 *           (req "counter" z)
 *           (req "gas_limit" z)
 *           (req "storage_limit" z)
 *           (req "ophash" Operation_hash.encoding))
 *     ) *)

module MempoolOperations = struct
  type t = Chain_db.Mempool_operations.t = {
    branch: Block_hash.t;
    ophash: Operation_hash.t;
    status: status;
    id: int;
    operation_kind: int;
    source: Signature.public_key_hash option;
    destination: Alpha_context.Contract.t option;
    seen: float;
    json_op: string Lazy.t;
  }

  let json_encoding =
    let open Data_encoding in
    let open Chain_db.Mempool_operations in
    conv
      (fun { branch ; ophash ; first_seen_level ; first_seen_timestamp
           ; last_seen_level ; last_seen_timestamp
           ; status ; id ; operation_kind
           ; source ; destination ; json_op ; block;  } ->
        (branch,
         ophash,
         first_seen_level,
         first_seen_timestamp,
         last_seen_level,
         last_seen_timestamp,
         Chain_db.Mempool_operations.string_of_status status,
         id,
         operation_kind,
         source),
        (destination,
         (match Data_encoding.Json.from_string json_op with
          | Ok json -> json
          | _ -> assert false),
         block)
      )
      (fun _ -> invalid_arg "Mempool_utils.json_encoding")
      (merge_objs
         (obj10
            (req "branch" Block_hash.encoding)
            (req "ophash" Operation_hash.encoding)
            (req "first_seen_level" int32)
            (req "first_seen_timestamp" int32)
            (req "last_seen_level" int32)
            (req "last_seen_timestamp" int32)
            (req "status" string)
            (req "id" int32)
            (req "operation_kind" string)
            (opt "source" Signature.Public_key_hash.encoding))
         (obj3
            (opt "destination" Alpha_context.Contract.encoding)
            (req "operation" json)
            (opt "block" Block_hash.encoding)
         )
      )


  module S = Set.Make(struct
      type t = Chain_db.Mempool_operations.t
      let compare a b =
        compare
          (a.ophash, a.id, a.status)
          (b.ophash, b.id, b.status)
    end)
  let s = ref S.empty
  let new_block () = s := S.empty
  let register ~pool
      ~json_op
      ~branch ?source ~id ~operation_kind ?destination ~status ophash =
    let seen = Unix.gettimeofday () in
    let open Chain_db.Mempool_operations in
    let d = { source ; branch ; operation_kind ; id ; destination ;
              status ; ophash ; seen ; json_op }
    in
    if S.mem d !s then
      Lwt.return (Ok ()) (* already added *)
    else
      begin
        (* register operation into map to avoid duplicates *)
        s:= S.add d !s;
        (* register operation into db *)
        Tezos_sql.find_opt pool
          Chain_db.Mempool_operations.insert d
        >>= Caqti_lwt.or_fail >>= fun _ ->
        return ()
      end

  let find ~pool ?pkh ?ophash () =
    ignore (pool, pkh, ophash);
    begin match ophash with
    | Some ophash ->
      Tezos_sql.collect_list pool
        Chain_db.Mempool_operations.select_by_ophash ophash
      >>= Caqti_lwt.or_fail >>= fun l ->
      return l
    | None ->
      match pkh with
      | Some pkh ->
        Tezos_sql.collect_list pool
          Chain_db.Mempool_operations.select_by_pkh pkh
        >>= Caqti_lwt.or_fail >>= fun l ->
        return l
      | None ->
        return []
    end


end

let int_of_contents :
  type a. a Alpha_context.contents -> int =
  function
  | Endorsement _ -> 0
  | Seed_nonce_revelation _ -> 1
  | Double_endorsement_evidence _ -> 2
  | Double_baking_evidence _ -> 3
  | Activate_account _ -> 4
  | Proposals _ -> 5
  | Ballot _ -> 6
  | Manager_operation { operation; _ } ->
    match operation with
    | Reveal _ -> 7
    | Transaction _ -> 8
    | Origination _ -> 9
    | Delegation _ -> 10

let rec process_contents :
  type a. ?id:int -> pool:_ -> branch:_ -> status:status -> ophash:_ -> a Alpha_context.contents_list -> unit =
  fun ?(id=0) ~pool ~branch ~status ~ophash cl ->
  match cl with
  | Alpha_context.Single e ->
    begin
      let json_op =
        (* we put this into a lazy value because we process each
           operation multiple times, therefore most of the time, we
           won't need this value *)
        lazy
          (Format.asprintf "%a"
             Data_encoding.Json.pp
             (Data_encoding.Json.construct Alpha_context.Operation.contents_encoding (Contents e)))
      in
      match e with
      | Manager_operation { source ; fee = _ ; operation = Reveal _ ; counter = _ ;
                            gas_limit = _ ; storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~json_op ~branch ~source  ~status ~id
              ~operation_kind:(int_of_contents e) ophash
            >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { source ; operation = Transaction { destination ; _ } ;
                            fee = _ ; counter = _ ;
                            gas_limit = _ ; storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~json_op ~branch ~source  ~status ~id ~destination
              ~operation_kind:(int_of_contents e) ophash
            >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { source ; fee = _ ; operation = Origination { delegate } ; counter = _ ;
                            gas_limit = _ ; storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~json_op ~branch ~source  ~status ~id
              ?destination:(Option.map Alpha_context.Contract.implicit_contract delegate)
              ~operation_kind:(int_of_contents e) ophash
            >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { source ; fee = _ ; operation = Delegation pkh_opt ; counter = _ ;
                            gas_limit = _ ; storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~json_op ~branch ~source  ~status ~id
              ?destination:(Option.map Alpha_context.Contract.implicit_contract pkh_opt)
              ~operation_kind:(int_of_contents e) ophash
            >>= fun _ -> Lwt.return_unit
          )
      | Activate_account { id = pkh ; activation_code = _ ; } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~json_op ~branch ~source:(Ed25519 pkh) ~status ~id
              ~operation_kind:(int_of_contents e) ophash
            >>= fun _ -> Lwt.return_unit
          )
      | Proposals { source ; period = _ ; proposals = _ ; } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) ophash
            >>= fun _ -> Lwt.return_unit
          )
      | Ballot { source ; period = _ ; proposal = _ ; ballot = _ ; } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) ophash
            >>= fun _ -> Lwt.return_unit
          )
      | Endorsement { level = _ ; }
      | Seed_nonce_revelation { level = _ ; nonce = _ ; }
      | Double_endorsement_evidence
          { op1 = { shell = { branch = _ } ;
                    protocol_data = { contents = _ ; signature = _ } }  ;
            op2 = { shell = { branch = _ } ;
                    protocol_data = { contents = _ ; signature = _ } }  ;
          }
      | Double_baking_evidence
          { bh1 = { shell = { level = _ ; proto_level = _ ; predecessor = _ ;
                              timestamp = _ ; validation_passes = _ ;
                              operations_hash = _ ; fitness = _ ; context = _ ;
                            } ;
                    protocol_data = { contents = _ ; signature = _ }
                  } ;
            bh2 = { shell = { level = _ ; proto_level = _ ; predecessor = _ ;
                              timestamp = _ ; validation_passes = _ ;
                              operations_hash = _ ; fitness = _ ; context = _ ;
                            } ;
                    protocol_data = { contents = _ ; signature = _ }
                  } } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~json_op ~branch ~status ~id
              ~operation_kind:(int_of_contents e) ophash
            >>= fun _ -> Lwt.return_unit
          )
    end
  | Cons (hd, tl) ->
    begin
      process_contents ~pool ~id ~status ~branch ~ophash (Single hd);
      process_contents ~pool ~id:(id+1) ~status ~branch ~ophash tl
    end


