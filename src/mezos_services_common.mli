(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* open Alpha_environment *)
open Tezos_protocol_007_PsDELPH1.Protocol
open Protocol
open Alpha_context
open Apply_results
open Protocol_client_context

open Environment


val history :
  ([ `GET ],
   unit,
   unit,
   Contract.contract list,
   unit,
   Chain_db.tx_full list list) RPC_service.service

val contracts :
  ([ `GET ],
   unit,
   unit,
   public_key_hash list,
   unit,
   Contract.t list list) RPC_service.service

val contracts2 :
  ([ `GET ],
   unit,
   unit,
   public_key_hash list,
   unit,
   Chain_db.Contract2.t list list) RPC_service.service

val contracts3 :
  ([ `GET ],
   unit,
   unit,
   public_key_hash list,
   unit,
   Chain_db.Contract3.t list list) RPC_service.service

val contracts_multisig :
  ([ `GET ],
   unit,
   unit,
   public_key list,
   unit,
   Chain_db.Contracts_multisig.t list list) RPC_service.service


val contract_info :
  ([ `GET ],
   unit,
   unit,
   Contract.t list,
   unit,
   Chain_db.Contract_table.t option list) RPC_service.service

val contract_info2 :
  ([ `GET ],
   unit,
   unit,
   Contract.t list,
   unit,
   Chain_db.Contract_table2.t option list) RPC_service.service


val create_sop : (* création d'instance d'opération *)
  ([ `POST ],
   unit,
   unit,
   unit,
   (Signature.public_key_hash * Signature.public_key * string * Signature.t),
   unit) RPC_service.service


val missing_blocks :
  ([ `GET ],
   unit,
   unit,
   unit,
   unit,
   int32 list) RPC_service.service

val manager :
  ([ `GET ],
   unit,
   unit,
   Contract.t list,
   unit,
   Chain_db.Manager.t option list) RPC_service.service


type operations_query = {
    address : Contract.t option;
    destination : Contract.t option;
    types : [ | `Reveal | `Transaction | `Origination | `Delegation ] list option;
    from: int64 option;
    downto_: int64 option;
    limit: int32 option;
    lastid: int32 option;
  }

val operations :
  ([ `GET ],
   unit,
   unit,
   operations_query,
   unit,
   Chain_db.Operations.t list) RPC_service.service


val mempool_ops :
  ([ `GET ],
   unit,
   unit,
   (Signature.Public_key_hash.t option * Operation_hash.t option),
   unit,
   Chain_db.Mempool_operations.select list) RPC_service.service

(* __RPC_SERVICE_SIG__ *)
