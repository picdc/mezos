(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Rresult
open Tezos_protocol_007_PsDELPH1.Protocol
open Environment
open Alpha_context

let k_arg =
  RPC_arg.make
    ~descr: "Contract"
    ~name: "contract"
    ~destruct:(fun s ->
        R.reword_error
          (fun e -> Format.asprintf "%a" pp_print_error e)
          (wrap_error (Contract.of_b58check s)))
    ~construct:Contract.to_b58check ()

let pkh_arg =
  RPC_arg.make
    ~descr: "Public key hash"
    ~name: "pkh"
    ~destruct:(fun s ->
        R.reword_error
          (fun exn_trap -> Format.asprintf "%a" R.pp_exn_trap exn_trap)
          (R.trap_exn Signature.Public_key_hash.of_b58check_exn s))
    ~construct:Signature.Public_key_hash.to_b58check ()

let ophash_arg =
  RPC_arg.make
    ~descr: "Operation hash"
    ~name: "ophash"
    ~destruct:(fun s ->
        R.reword_error
          (fun exn_trap -> Format.asprintf "%a" R.pp_exn_trap exn_trap)
          (R.trap_exn Operation_hash.of_b58check_exn s))
    ~construct:Operation_hash.to_b58check ()

let pk_arg =
  RPC_arg.make
    ~descr: "Public key"
    ~name: "pk"
    ~destruct:(fun s ->
      R.reword_error
        (fun exn_trap -> Format.asprintf "%a" R.pp_exn_trap exn_trap)
        (R.trap_exn Signature.Public_key.of_b58check_exn s))
    ~construct:Signature.Public_key.to_b58check ()


(* let sig_arg =
 *   RPC_arg.make
 *     ~descr: "Signature"
 *     ~name: "signature"
 *     ~destruct:(fun s ->
 *       R.reword_error
 *         (fun exn_trap -> Format.asprintf "%a" R.pp_exn_trap exn_trap)
 *         (R.trap_exn Signature.of_b58check_exn s))
 *     ~construct:Signature.to_b58check () *)

(* let data_arg =
 *   RPC_arg.make
 *     ~descr: "data"
 *     ~name: "binary"
 *     ~destruct:(fun s ->
 *       R.reword_error
 *         (fun exn_trap -> Format.asprintf "%a" R.pp_exn_trap exn_trap)
 *         (R.trap_exn (fun x -> x) s))
 *     ~construct:(fun x -> x) () *)


let ks_query =
  let open RPC_query in
  query (fun a -> a)
  |+ multi_field
    ~descr:"Contracts to get history for" "ks" k_arg (fun a -> a)
  |> seal

let history =
  RPC_service.get_service
    ~description: "Get the history of contracts"
    ~query:ks_query
    ~output:Data_encoding.(list (list Chain_db.tx_full_encoding))
    RPC_path.(root / "history")

let contracts =
  let query =
    let open RPC_query in
    query (fun a -> a)
    |+ multi_field ~descr:"pkhs" "mgrs" pkh_arg (fun a -> a)
    |> seal in
  RPC_service.get_service
    ~description: "Get contracts managed by mgr"
    ~query
    ~output:Data_encoding.(list (list Contract.encoding))
    RPC_path.(root / "contracts")


let contracts2 =
  let query =
    let open RPC_query in
    query (fun a -> a)
    |+ multi_field ~descr:"pkhs" "mgrs" pkh_arg (fun a -> a)
    |> seal in
  RPC_service.get_service
    ~description: "Get contracts with diff, managed by mgr"
    ~query
    ~output:Data_encoding.(list (list Chain_db.Contract2.json_encoding))
    RPC_path.(root / "contracts2")

let contracts3 =
  let query =
    let open RPC_query in
    query (fun a -> a)
    |+ multi_field ~descr:"pkhs" "mgrs" pkh_arg (fun a -> a)
    |> seal in
  RPC_service.get_service
    ~description: "Get contracts with diff, managed by mgr"
    ~query
    ~output:Data_encoding.(list (list Chain_db.Contract3.json_encoding))
    RPC_path.(root / "contracts3")

let contracts_multisig =
  let query =
    let open RPC_query in
    query (fun a -> a)
    |+ multi_field ~descr:"pks" "pks" pk_arg (fun a -> a)
    |> seal in
  RPC_service.get_service
    ~description: "Get contracts to be signed"
    ~query
    ~output:Data_encoding.(list (list Chain_db.Contracts_multisig.json_encoding))
    RPC_path.(root / "multisig")

let contract_info =
  RPC_service.get_service
    ~description: "Get delegate status of contracts"
    ~query:ks_query
    ~output:Data_encoding.(list (option Chain_db.Contract_table.json_encoding))
    RPC_path.(root / "contract_info")

let contract_info2 =
  RPC_service.get_service
    ~description: "Get delegate status of contracts"
    ~query:ks_query
    ~output:Data_encoding.(list (option Chain_db.Contract_table2.json_encoding))
    RPC_path.(root / "contract_info2")

(* type create_operation = {
 *     src: Signature.Public_key_hash.t;
 *     src_pk: Signature.Public_key.t;
 *     binary: string;
 *     signature: Signature.t;
 *   } *)

let create_sop =
  let open Data_encoding in
  RPC_service.post_service
    ~description: "Create operation instance"
    ~query:RPC_query.empty
    ~input:(obj4
              (req "src" Signature.Public_key_hash.encoding)
              (req "src_pk" Signature.Public_key.encoding)
              (req "binary" Data_encoding.string)
              (req "signature" Signature.encoding)
    )
    ~output:Data_encoding.empty
    RPC_path.(root / "create_sop")

let missing_blocks =
  RPC_service.get_service
    ~description: "Find missing blocks in DB"
    ~query:RPC_query.empty
    ~output:Data_encoding.(list int32)
    RPC_path.(root / "missing_blocks")

let manager =
  let query =
    let open RPC_query in
    query (fun a -> a)
    |+ multi_field ~descr:"ks" "ks" k_arg (fun a -> a)
    |> seal in
  RPC_service.get_service
    ~description: "Get manager of a contract"
    ~query
    ~output:Data_encoding.(list (option Chain_db.Manager.json_encoding))
    RPC_path.(root / "manager")

(* ********************************************************************** *)
(* Get operations *)
let types_arg =
  let deconstruct = function s ->
    List.fold_left
      (fun r -> function
         | "reveal" -> `Reveal :: r
         | "transaction" -> `Transaction :: r
         | "origination" -> `Origination :: r
         | "delegation" -> `Delegation :: r
         | _ -> r)
      []
      (String.split_on_char ',' s)
  in
  let construct l =
    List.map
      (function
        | `Reveal -> "reveal"
        | `Transaction -> "transaction"
        | `Origination -> "origination"
        | `Delegation -> "delegation")
      l
    |> String.concat ","
  in
  RPC_arg.make
    ~descr: "operation type"
    ~name: "operation type"
    ~destruct:(fun s ->
      R.reword_error
        (fun exn_trap -> Format.asprintf "%a" R.pp_exn_trap exn_trap)
        (R.trap_exn deconstruct s))
    ~construct
    ()

let int32_arg =
  RPC_arg.make
    ~descr: "int32"
    ~name: "int32"
    ~destruct:(fun s ->
      R.reword_error
        (fun exn_trap -> Format.asprintf "%a" R.pp_exn_trap exn_trap)
        (R.trap_exn Stdlib.Int32.of_string s))
    ~construct:Stdlib.Int32.to_string
    ()

(* let int64_arg =
 *   RPC_arg.make
 *     ~descr: "int64"
 *     ~name: "int64"
 *     ~destruct:(fun s ->
 *       R.reword_error
 *         (fun exn_trap -> Format.asprintf "%a" R.pp_exn_trap exn_trap)
 *         (R.trap_exn Int64.of_string s))
 *     ~construct:Int64.to_string
 *     () *)

(* let time_arg =
 *   Tezos_base__TzPervasives.Time.Protocol.rpc_arg *)

let time_arg =
  let module Time = Tezos_base__TzPervasives.Time in
  RPC_arg.make
    ~descr: "Time RFC3339"
    ~name: "Time"
    ~destruct:(fun s ->
      R.reword_error
        (fun exn_trap -> Format.asprintf "%a" R.pp_exn_trap exn_trap)
        (R.trap_exn
           (fun e -> Time.Protocol.of_notation_exn e |> Time.Protocol.to_seconds)
           s))
    ~construct:(fun e -> Int64.to_string e)
    ()



type operations_query = {
    address : Contract.t option;
    destination : Contract.t option;
    types : [ | `Delegation | `Origination | `Reveal | `Transaction ] list option;
    from: int64 option;
    downto_: int64 option;
    limit: int32 option;
    lastid: int32 option;
  }

let operations =
  let query =
    let open RPC_query in
    query
      (fun address destination types from downto_ limit lastid ->
         {address; destination; types; from = max from downto_; downto_ = min from downto_;
          limit; lastid})
    |+ opt_field ~descr:"address" "address" k_arg (fun a -> a.address)
    |+ opt_field ~descr:"destination" "destination" k_arg (fun a -> a.destination)
    |+ opt_field ~descr:"types" "types" types_arg (fun a -> a.types)
    |+ opt_field ~descr:"from" "from" time_arg (fun a -> max a.from a.downto_)
    |+ opt_field ~descr:"to" "to" time_arg (fun a -> min a.from a.downto_)
    |+ opt_field ~descr:"limit" "limit" int32_arg (fun a -> a.limit)
    |+ opt_field ~descr:"last ID" "lastid" int32_arg (fun a -> a.lastid)
    |> seal in
  RPC_service.get_service
    ~description: "Get operations"
    ~query
    ~output:Data_encoding.(list (Chain_db.Operations.json_encoding))
    RPC_path.(root / "operations")

(* ********************************************************************** *)

let mempool_ops =
  let query =
    let open RPC_query in
    query
      (fun pkh ophash -> pkh, ophash)
    |+ opt_field ~descr:"pkh" "pkh" pkh_arg (fun (a, _) -> a)
    |+ opt_field ~descr:"ophash" "ophash" ophash_arg (fun (_, a) -> a)
    |> seal in
  RPC_service.get_service
    ~description: "Get operations from mempool"
    ~query
    ~output:Data_encoding.(list (Mempool_utils.MempoolOperations.json_encoding))
    RPC_path.(root / "mempool_operations")


(* __RPC_SERVICE__ *)
