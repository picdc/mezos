all:tezos-indexer tezos
	@echo run \`${MAKE} 6\` to compile for PsCARTHA or \`${MAKE} 7\` to compile for PsDELPH1

tezos-indexer:
	git clone --single-branch https://gitlab.com/nomadic-labs/tezos-indexer.git -b v6.1.0
	cd $@ && ${MAKE} protos


tezos:
	git clone --single-branch https://gitlab.com/philippewang.info/tezos.git -b v7.4-mezos

GENERATED = src/mezos.ml src/chain_db.mli src/chain_db.ml src/mezos_transfer.ml


6:
	echo 'let proto = "006_PsCARTHA"' > env.ml.mpp
	${MAKE} ${GENERATED}
	find src -iname '*.ml' -or -iname '*.mli' | while read l ; do if grep -q -E '(PsDELPH1|PtEdoTez)' $$l ; then sed -e 's/00.-P......./006-PsCARTHA/g' -e 's/00._P......./006_PsCARTHA/g' $$l > $$l.tmp ; mv $$l.tmp $$l ; fi ; done
	TEZOS_PROTO=6 dune build @install
	cp -f _build/default/src/mezos.exe ./ || cp $$(find _build -name mezos.exe) ./

7:
	echo 'let proto = "007_PsDELPH1"' > env.ml.mpp
	${MAKE} ${GENERATED}
	find src -iname '*.ml' -or -iname '*.mli' | while read l ; do if grep -q -E '(PsCARTHA|PtEdoTez)' $$l ; then sed -e 's/00.-P......./007-PsDELPH1/g' -e 's/00._P......./007_PsDELPH1/g' $$l > $$l.tmp ; mv $$l.tmp $$l ; fi ; done
	TEZOS_PROTO=7 dune build @install
	cp -f _build/default/src/mezos.exe ./ || cp $$(find _build -name mezos.exe) ./

8:
	echo 'let proto = "008_PtEdoTez"' > env.ml.mpp
	${MAKE} ${GENERATED}
	find src -iname '*.ml' -or -iname '*.mli' | while read l ; do if grep -q -E '(PsDELPH1|PsCARTHA)' $$l ; then sed -e 's/00.-P......./008-PtEdoTez/g' -e 's/00._P......./008_PtEdoTez/g' $$l > $$l.tmp ; mv $$l.tmp $$l ; fi ; done
	TEZOS_PROTO=8 dune build @install
	cp -f _build/default/src/mezos.exe ./ || cp $$(find _build -name mezos.exe) ./



# use OCaml as a preprocessor language, via MPP:
MPP = mpp -l ocaml -so '' -sc '' -t ocaml  -son '' -scn '' -sos-noloc '' -soc '' -scc '' -sec '' -sos "(**\#" -scs "\#**)"

src/mezos.ml:src/mezos.ml.mpp Makefile env.ml.mpp
	(cat env.ml.mpp && ${MPP} $<) > tmp.ml
	ocaml tmp.ml > $@
	rm -f tmp.ml

src/chain_db.ml:src/chain_db.ml.mpp Makefile env.ml.mpp
	(cat env.ml.mpp && ${MPP} $<) > tmp2.ml
	ocaml tmp2.ml > $@
	rm -f tmp2.ml

src/chain_db.mli:src/chain_db.mli.mpp Makefile env.ml.mpp
	(cat env.ml.mpp && ${MPP} $<) > tmp3.ml
	ocaml tmp3.ml > $@
	rm -f tmp3.ml

src/mezos_transfer.ml:src/mezos_transfer.ml.mpp Makefile env.ml.mpp
	(cat env.ml.mpp && ${MPP} $<) > tmp4.ml
	ocaml tmp4.ml > $@
	rm -f tmp4.ml



# 4:
# 	TEZOS_PROTO=4 dune build @install
# 3:
# 	TEZOS_PROTO=3 dune build @install
# 2:
# 	TEZOS_PROTO=2 dune build @install
# 1:
# 	TEZOS_PROTO=1 dune build @install

clean:
	dune clean

distclean:
	rm -fr tezos-indexer/ tezos/


updatedb:tezos-indexer/src/mezos-db/db.sql tezos-indexer/src/mezos-db/gen_sql_queries.ml
	(cd tezos-indexer && ${MAKE} -s db-scheme-mezos) | psql 2>&1 | tee updatedb
	if grep -q ERROR updatedb ; then rm updatedb ; fi

updatedb-force:
	rm -f updatedb
	${MAKE} updatedb
